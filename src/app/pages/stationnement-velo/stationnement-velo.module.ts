import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { StationnementVeloPage } from './stationnement-velo.page';

const routes: Routes = [
  {
    path: '',
    component: StationnementVeloPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [StationnementVeloPage]
})
export class StationnementVeloPageModule {}
