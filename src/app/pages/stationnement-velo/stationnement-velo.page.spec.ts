import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StationnementVeloPage } from './stationnement-velo.page';

describe('StationnementVeloPage', () => {
  let component: StationnementVeloPage;
  let fixture: ComponentFixture<StationnementVeloPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StationnementVeloPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StationnementVeloPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
