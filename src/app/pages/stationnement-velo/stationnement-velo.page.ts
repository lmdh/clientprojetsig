import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ActionSheetController, ModalController} from '@ionic/angular';
import {StationnementVeloDto} from '../../dto/StationnementVeloDto';
import {ModalAddDegradationPage} from '../../modals/modal-add-degradation/modal-add-degradation.page';
import {LayerType} from '../../enums/LayerType';
import {ModalDegradationListPage} from '../../modals/modal-degradation-list/modal-degradation-list.page';

@Component({
    selector: 'app-stationnement-velo',
    templateUrl: './stationnement-velo.page.html',
    styleUrls: ['./stationnement-velo.page.scss'],
})
export class StationnementVeloPage implements OnInit {

    stationnementVelo: StationnementVeloDto;
    adress: string;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private actionSheetController: ActionSheetController,
                private modalController: ModalController) {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.stationnementVelo = this.router.getCurrentNavigation().extras.state.stationnementVelo;
            }
        });
    }

    ngOnInit() {
        if (this.stationnementVelo !== null && this.stationnementVelo.rue !== null && this.stationnementVelo.commune !== null) {
            this.adress = this.stationnementVelo.numero + ' ' + this.stationnementVelo.rue + ' ' + this.stationnementVelo.commune;
        } else {
            this.adress = this.stationnementVelo.commune;
        }
    }

    /**
     * Méthode permettant d'afficher la fiche d'action de signalement.
     */
    async presentSignalementActionSheet() {
        const actionSheet = await this.actionSheetController.create({
            header: 'Effectuer un signalement',
            buttons: [{
                text: 'Signaler une dégradation',
                handler: () => {
                    this.presentAddDegradationModal();
                }
            }, {
                text: 'Annuler',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    console.log('Cancel clicked');
                }
            }]
        });
        await actionSheet.present();
    }

    /**
     * Fonction permettant de présenter la page d'ajout de dégradation.
     */
    private async presentAddDegradationModal() {
        const modal = await this.modalController.create({
            component: ModalAddDegradationPage,
            componentProps: {
                id: this.stationnementVelo.numero,
                title: this.stationnementVelo.type + ' | ' + this.adress,
                degradationType: LayerType.StationnementVelo
            }
        });
        return await modal.present();
    }

    /**
     * Fonction permettant de présenter la page contenant la liste des dégradations
     */
    private async presentDegradationListModal() {
        const modal = await this.modalController.create({
            component: ModalDegradationListPage,
            componentProps: {
                id: this.stationnementVelo.numero,
                title: this.stationnementVelo.type + ' | ' + this.adress,
                degradationType: LayerType.StationnementVelo
            }
        });
        return await modal.present();
    }
}
