import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertController} from '@ionic/angular';
import {Router} from '@angular/router';
import {AuthRequestDto} from '../../dto/AuthRequestDto';
import {AuthResultDto} from '../../dto/AuthResultDto';
import {RestApiService} from '../../services/rest-api.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

    public registerForm: FormGroup;

    errorMessages = {
        confirmPassword: [
            {type: 'notSame', message: 'Le mot de passe ne correspond pas'}
        ]
    };

    constructor(
        private restApiService: RestApiService,
        private alertController: AlertController,
        private router: Router,
        public formBuilder: FormBuilder) {
    }

    ngOnInit() {
        // On définit les champs du formulaire et on indique si ils sont requis.
        this.registerForm = this.formBuilder.group({
            login: ['', Validators.required],
            password: ['', Validators.required],
            confirmPassword: new FormControl('', [Validators.required])
        }, {validators: this.checkPasswords});
    }

    /**
     * Méthode de validation de formulaire qui va permettre de vérifier que le mdp et le mdp de confirmation sont identiques.
     * @param group Le formulaire qui va être validé.
     */
    checkPasswords(group: FormGroup) {
        const password = group.get('password');
        const confirmPassword = group.get('confirmPassword');
        // Si les deux mdps sont différents on set l'errreur notSame
        if (password.value !== confirmPassword.value) {
            confirmPassword.setErrors({notSame: true});
        } else {
            confirmPassword.setErrors(null);
        }
        return null;
    }

    /**
     * Fonction permettant de s'inscrire.
     */
    public register() {
        const authRequest: AuthRequestDto = new AuthRequestDto();
        authRequest.login = this.registerForm.get('login').value;
        authRequest.password = this.registerForm.get('password').value;
        this.restApiService.postRegister(authRequest).subscribe(authResult => {
                this.registerResult(authResult);
            },
            error => {
                this.registerError(error);
            });
    }

    /**
     * Permet d'afficher une alert box contenant le message d'erreur passé en paramètre.
     * @param error Le message d'erreur à afficher.
     */
    private registerError(error: any) {
        this.alertController.create({
            header: 'Echec',
            message: error,
            buttons: ['Ok']
        }).then(alert => alert.present());
        this.registerForm.reset();
    }

    /**
     * Fonction permettant d'afficher l'alert de confirmation d'inscription et de rediriger vers la page d'acceuil.
     * @param authResult Le dto retourné qui contient des tokens.
     */
    private registerResult(authResult: AuthResultDto) {
            this.restApiService.setTokens(authResult);
            this.router.navigate(['/home']).then(() => {
                    this.alertController.create({
                        header: 'Succès',
                        message: 'Inscription réussie !',
                        buttons: ['Ok']
                    }).then(alert => alert.present());
                }
            );
    }
}
