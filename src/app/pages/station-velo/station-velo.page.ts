import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ActionSheetController, ModalController} from '@ionic/angular';
import {StationVeloDto} from '../../dto/StationVeloDto';
import {ModalDegradationListPage} from '../../modals/modal-degradation-list/modal-degradation-list.page';
import {ModalAddPenuriePage} from '../../modals/modal-add-penurie/modal-add-penurie.page';
import {ModalPenurieListPage} from '../../modals/modal-penurie-list/modal-penurie-list.page';
import {ModalAddDegradationPage} from '../../modals/modal-add-degradation/modal-add-degradation.page';
import {LayerType} from '../../enums/LayerType';

@Component({
    selector: 'app-station-velo',
    templateUrl: './station-velo.page.html',
    styleUrls: ['./station-velo.page.scss'],
})
export class StationVeloPage implements OnInit {

    private stationVelo: StationVeloDto;
    public title: string;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private actionSheetController: ActionSheetController,
                private modalController: ModalController) {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.stationVelo = this.router.getCurrentNavigation().extras.state.stationVelo;
            }
        });
    }

    ngOnInit() {
        console.log(this.stationVelo);
        this.title = this.stationVelo.nomstation;
    }

    /**
     * Méthode permettant d'afficher la fiche d'action de signalement.
     */
    async presentSignalementActionSheet() {
        const actionSheet = await this.actionSheetController.create({
            header: 'Effectuer un signalement',
            buttons: [{
                text: 'Signaler une pénurie',
                handler: () => {
                    this.presentAddPenurieModal();
                }
            }, {
                text: 'Signaler une dégradation',
                handler: () => {
                    this.presentAddDegradationModal();
                }
            }, {
                text: 'Annuler',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    console.log('Cancel clicked');
                }
            }]
        });
        await actionSheet.present();
    }

    /**
     * Fonction permettant de présenter la page contenant la liste des pénuries.
     */
    public async presentPenurieListModal() {
        const modal = await this.modalController.create({
            component: ModalPenurieListPage,
            componentProps: {
                station: this.stationVelo
            }
        });
        return await modal.present();
    }

    /**
     * Fonction permettant de présenter la page contenant la liste des dégradations.
     */
    public async presentDegradationListModal() {
        const modal = await this.modalController.create({
            component: ModalDegradationListPage,
            componentProps: {
                id: this.stationVelo.id,
                title: this.title,
                degradationType: LayerType.StationVelo
            }
        });
        return await modal.present();
    }

    /**
     * Fonction permettant de présenter la page d'ajout de dégradation.
     */
    private async presentAddDegradationModal() {
        const modal = await this.modalController.create({
            component: ModalAddDegradationPage,
            componentProps: {
                id: this.stationVelo.id,
                title: this.title,
                degradationType: LayerType.StationVelo
            }
        });
        return await modal.present();
    }

    /**
     * Fonction permettant de présenter la page d'ajout de pénurie.
     */
    private async presentAddPenurieModal() {
        const modal = await this.modalController.create({
            component: ModalAddPenuriePage,
            componentProps: {
                station: this.stationVelo
            }
        });
        return await modal.present();
    }
}
