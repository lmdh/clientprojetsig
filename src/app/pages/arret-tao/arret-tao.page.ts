import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ArretTaoDto} from '../../dto/ArretTaoDto';
import {ActionSheetController, ModalController} from '@ionic/angular';
import {ModalAddRetardPage} from '../../modals/modal-add-retard/modal-add-retard.page';
import {ModalRetardListPage} from '../../modals/modal-retard-list/modal-retard-list.page';
import {ModalAddCoupureLignePage} from '../../modals/modal-add-coupure-ligne/modal-add-coupure-ligne.page';
import {LayerType} from '../../enums/LayerType';
import {ModalCoupureListPage} from '../../modals/modal-coupure-list/modal-coupure-list.page';
import {ModalAddDegradationPage} from '../../modals/modal-add-degradation/modal-add-degradation.page';
import {ModalDegradationListPage} from '../../modals/modal-degradation-list/modal-degradation-list.page';

@Component({
    selector: 'app-arret-tao',
    templateUrl: './arret-tao.page.html',
    styleUrls: ['./arret-tao.page.scss'],
})
export class ArretTaoPage implements OnInit {

    private arretTao: ArretTaoDto;
    private routeId: string;
    public arretType: string;
    public layerType: LayerType;
    public title: string;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private actionSheetController: ActionSheetController,
                private modalController: ModalController) {
        // On récupère les données transmises durant la navigation
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.arretTao = this.router.getCurrentNavigation().extras.state.arretTao;
                this.routeId = this.router.getCurrentNavigation().extras.state.routeId;
                this.layerType = this.router.getCurrentNavigation().extras.state.layerType;
            }
        });
    }

    ngOnInit() {
        if (this.layerType === LayerType.Bus) {
            this.arretType = 'Arrêt de bus';
        } else {
            this.arretType = 'Arrêt de tramway';
        }
        this.title = this.arretType + ' | Ligne ' + this.routeId.substring(3, this.routeId.length);
    }

    /**
     * Fonction permettant de présenter la page d'ajout de retard.
     */
    async presentAddRetardModal() {
        const modal = await this.modalController.create({
            component: ModalAddRetardPage,
            componentProps: {
                gid: this.arretTao.gid,
                routeId: this.routeId,
                arretType: this.arretType,
                stopname: this.arretTao.stopname
            }
        });
        return await modal.present();
    }

    /**
     * Fonction permettant de présenter la page contenant la liste des retards du jour.
     */
    async presentRetardListModal() {
        const modal = await this.modalController.create({
            component: ModalRetardListPage,
            componentProps: {
                routeId: this.routeId,
                arretId: this.arretTao.gid,
                arretType: this.arretType,
                stopname: this.arretTao.stopname            }
        });
        return await modal.present();
    }

    /**
     * Fonction permettant de présenter la page d'ajout de coupure de ligne.
     */
    async presentAddCoupureLigneModal() {
        const modal = await this.modalController.create({
            component: ModalAddCoupureLignePage,
            componentProps: {
                routeId: this.routeId,
            }
        });
        return await modal.present();
    }

    /**
     * Fonction permettant de présenter la page contenant la liste des coupures de ligne.
     */
    async presentCoupureListModal() {
        const modal = await this.modalController.create({
            component: ModalCoupureListPage,
            componentProps: {
                routeId: this.routeId,
            }
        });
        return await modal.present();
    }


    /**
     * Fonction permettant de présenter la page d'ajout de dégradation.
     */
    async presentAddDegradationModal() {
        const degradationTitle = this.arretType + ' ' + this.arretTao.stopname + ' | Ligne ' + this.routeId.substring(3, this.routeId.length);
        const modal = await this.modalController.create({
            component: ModalAddDegradationPage,
            componentProps: {
                id: this.arretTao.gid,
                title: degradationTitle,
                degradationType: this.layerType
            }
        });
        return await modal.present();
    }

    /**
     * Fonction permettant de présenter la page contenant la liste des dégradations.
     */
    async presentDegradationListModal() {
        const degradationTitle = this.arretType + ' ' + this.arretTao.stopname + ' | Ligne ' + this.routeId.substring(3, this.routeId.length);
        const modal = await this.modalController.create({
            component: ModalDegradationListPage,
            componentProps: {
                id: this.arretTao.gid,
                title: degradationTitle,
                degradationType: this.layerType
            }
        });
        return await modal.present();
    }

    /**
     * Méthode permettant d'afficher la fiche d'action de signalement
     */
    async presentSignalementActionSheet() {
        const actionSheet = await this.actionSheetController.create({
            header: 'Effectuer un signalement',
            buttons: [{
                text: 'Signaler un retard',
                handler: () => {
                    this.presentAddRetardModal();
                }
            }, {
                text: 'Signaler une coupure',
                handler: () => {
                    this.presentAddCoupureLigneModal();
                }
            }, {
                text: 'Signaler une dégradation',
                handler: () => {
                    this.presentAddDegradationModal();
                }
            }, {
                text: 'Annuler',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    console.log('Cancel clicked');
                }
            }]
        });
        await actionSheet.present();
    }
}
