import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ArretTaoPage } from './arret-tao.page';
import {ModalAddRetardPageModule} from '../../modals/modal-add-retard/modal-add-retard.module';

const routes: Routes = [
  {
    path: '',
    component: ArretTaoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ArretTaoPage]
})
export class ArretTaoPageModule {}
