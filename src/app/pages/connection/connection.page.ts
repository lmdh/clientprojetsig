import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthRequestDto} from '../../dto/AuthRequestDto';
import {AuthResultDto} from '../../dto/AuthResultDto';
import {AlertController} from '@ionic/angular';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RestApiService} from '../../services/rest-api.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-connection',
    templateUrl: './connection.page.html',
    styleUrls: ['./connection.page.scss'],
})
export class ConnectionPage implements OnInit, OnDestroy {

    public loginForm: FormGroup;
    private subscription: Subscription;

    constructor(
        private restApiService: RestApiService,
        private alertController: AlertController,
        private router: Router,
        public formBuilder: FormBuilder) {
        // On définit les champs du formulaire et on indique si ils sont requis.
        this.loginForm = this.formBuilder.group({
            login: ['', Validators.required],
            password: ['', Validators.required]
        });
    }


    ngOnInit() {
    }

    ngOnDestroy() {
        if (this.subscription != null) {
            this.subscription.unsubscribe();
        }
    }

    /**
     * Fonction permettant d'afficher l'alert de confirmation de connexion et de rediriger vers la page d'acceuil.
     * @param authResult Le dto retourné qui contient des tokens.
     */
    private connexionResult(authResult: AuthResultDto) {
        this.restApiService.setTokens(authResult);
        this.router.navigate(['/home']).then(() => {
                this.alertController.create({
                    header: 'Succès',
                    message: 'Connexion réussie !',
                    buttons: ['Ok']
                }).then(alert => alert.present());
            }
        );
    }

    /**
     * Permet d'afficher une alert box contenant le message d'erreur passé en paramètre
     * @param error Le message d'erreur à afficher
     */
    private connectionError(error: any) {
        this.alertController.create({
            header: 'Echec',
            message: error,
            buttons: ['Ok']
        }).then(alert => alert.present());
        this.loginForm.reset();
    }


    /**
     * Fonction permttant de se connecter
     */
    public connexion() {
        const authRequest: AuthRequestDto = new AuthRequestDto();
        authRequest.login = this.loginForm.get('login').value;
        authRequest.password = this.loginForm.get('password').value;
        this.subscription = this.restApiService.postLogin(authRequest).subscribe(authResult => {
            this.connexionResult(authResult);
        }, error => {
            this.connectionError(error);
        });
    }
}
