import {Component, OnInit} from '@angular/core';
import {ArretTaoDto} from '../../dto/ArretTaoDto';
import {NavigationExtras, Router} from '@angular/router';
import {ActionSheetController, ModalController} from '@ionic/angular';
import {LayerType} from '../../enums/LayerType';
import {LayerFactory} from '../../LayerFactory';
import {OSM, TileWMS} from 'ol/source';
import OlMap from 'ol/Map';
import TileLayer from 'ol/layer/Tile';
import {MapBrowserEvent, View} from 'ol';
import {fromLonLat} from 'ol/proj';
import {GestionPositionService} from '../../services/gestion-position.service';
import {StationVeloDto} from '../../dto/StationVeloDto';
import {StationnementVeloDto} from '../../dto/StationnementVeloDto';
import {ModalPistesCyclablesListPage} from '../../modals/modal-pistes-cyclables-list/modal-pistes-cyclables-list.page';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {


    public sources: Map<LayerType, TileWMS> = new Map();
    public layers: Map<LayerType, TileLayer> = new Map();
    private map: OlMap;
    private view: View;
    private longitude: number;
    private latitude: number;
    private isClicked: boolean;

    toggleList: any = [
        {
            label: 'Arrêts de bus',
            type: LayerType.Bus,
            isToggled: false
        }, {
            label: 'Arrêts de tramway',
            type: LayerType.Tramway,
            isToggled: false
        }, {
            label: 'Stations de vélo',
            type: LayerType.StationVelo,
            isToggled: false
        }, {
            label: 'Stationnements vélo',
            type: LayerType.StationnementVelo,
            isToggled: false
        }
    ];

    constructor(
        private router: Router,
        private actionSheetController: ActionSheetController,
        private positionService: GestionPositionService,
        private modalController: ModalController) {
    }

    /**
     * Méthode permettant de récupérer le premier élément du json parsé
     * @param pointInfo Le string qui contient les infos du point sur lequel on a cliqué.
     */
    private static getFirstElement(pointInfo: any) {
        return pointInfo.features[0];
    }

    async ngOnInit(): Promise<void> {
        const osm = new TileLayer({
            source: new OSM({
                opaque: false
            })
        });
        // On crée les sources et les couches à l'aide de la classe LayerFactory
        const layerFactory: LayerFactory = new LayerFactory();
        this.sources.set(LayerType.Bus, layerFactory.getSource(LayerType.Bus));
        this.sources.set(LayerType.Tramway, layerFactory.getSource(LayerType.Tramway));
        this.sources.set(LayerType.StationVelo, layerFactory.getSource(LayerType.StationVelo));
        this.sources.set(LayerType.StationnementVelo, layerFactory.getSource(LayerType.StationnementVelo));
        this.layers.set(LayerType.Bus, layerFactory.getLayer(this.sources.get(LayerType.Bus)));
        this.layers.set(LayerType.Tramway, layerFactory.getLayer(this.sources.get(LayerType.Tramway)));
        this.layers.set(LayerType.StationVelo, layerFactory.getLayer(this.sources.get(LayerType.StationVelo)));
        this.layers.set(LayerType.StationnementVelo, layerFactory.getLayer(this.sources.get(LayerType.StationnementVelo)));
        await this.getPosition();
        this.view = new View({
            center: fromLonLat([this.longitude, this.latitude]),
            // center: fromLonLat([1.909251, 47.902964]),
            zoom: 15
        });
        this.map = new OlMap({
            target: 'map',
            layers: [osm, this.layers.get(LayerType.Bus), this.layers.get(LayerType.Tramway),
                     this.layers.get(LayerType.StationVelo), this.layers.get(LayerType.StationnementVelo)],
            view: this.view
        });

        // Listener qui permet de gérer les clicks de l'utilisateur sur la map
        this.map.addEventListener('singleclick', (event: MapBrowserEvent) => {
            return this.mapClick(event);
        });
        this.layers.forEach((value: TileLayer) => {
            value.setVisible(false);
        });
    }

    /**
     * Méthode permettant d'extraire le le numero de ligne contenu dans le stopId
     * @param stopId StopId qui contient le numero de ligne
     */
    private getRouteId(stopId: string) {
        const regex = /(?<=:).*?(?=(?:_|$))/g;
        const res: string[] = stopId.match(regex);
        if (res) {
            return res[0];
        } else {
            return '';
        }
    }

    /**
     * Méthode appelée lorsque l'utilisateur clique sur la map
     * @param event L'événement du clic
     */
    private mapClick(event: MapBrowserEvent) {
        this.isClicked = false;
        const viewResolution = /** {number} */ (this.view.getResolution());
        // On parcours toutes les sources pour voir s'il y a un élément à l'endroit ou on a cliqué
        this.sources.forEach((source, type) => {
            // On recherche un élément uniquement dans les couches visibles
            if (this.layers.get(type) !== undefined && this.layers.get(type).getVisible()) {
                const url = source.getFeatureInfoUrl(
                    event.coordinate,
                    viewResolution,
                    'EPSG:3857',
                    {INFO_FORMAT: 'application/json'}
                );
                if (url) {
                    fetch(url)
                        .then(response => {
                            return response.text();
                        })
                        .then(jsonResponse => {
                            const parsedJson: any = JSON.parse(jsonResponse);
                            // On vérifie que le json retourné contient au moins un élément
                            if (this.jsonResponseIsValid(parsedJson)) {
                                // On vérifie qu'on a pas déjà cliqué sur un autre élément
                                if (!this.isClicked) {
                                    this.isClicked = true;
                                    this.showDetail(parsedJson);
                                }
                            }
                        });
                }
            }
        });
        return false;
    }

    ionViewWillEnter() {
        this.map.updateSize();
    }

    /**
     * Retourne vrai si le json contient au moins un point (un arrêt ou une station de vélo par exemple), retourne faux sinon.
     * @param jsonResponse Le json que l'on va vérifier
     */
    private jsonResponseIsValid(jsonResponse: any) {
        return Array.isArray(jsonResponse.features) && jsonResponse.features.length;
    }

    /**
     * Fonction permettant de naviguer la page de detail d'un arrêt Tao, d'une station vélo ou d'un stationnement vélo
     * @param layerType Tyoe de la couche (arrêt Tao, station vélo ou stationnement vélo)
     * @param navigationExtras Données qui seront passées durant la navigation (un dto par exemple)
     */
    private openDetailPage(layerType: LayerType, navigationExtras: NavigationExtras) {
        let route: string;
        switch (layerType) {
            case LayerType.Bus :
            case LayerType.Tramway:
                route = 'arret-tao';
                break;
            case LayerType.StationVelo:
                route = 'station-velo';
                break;
            case LayerType.StationnementVelo:
                route = 'stationnement-velo';
                break;
            default:
                route = '';
                break;
        }
        this.router.navigate([route], navigationExtras).then();
    }

    /**
     * Fonction permettant de présenter les infos de l'élément sur lequel on a cliqué.
     * @param layerType Tyoe de l'élément sur lequel on a cliqué.
     * @param title Titre qui sera affiché dans l'action sheet.
     * @param navigationExtras Données qui seront passées durant la navigation vers la page de detail de l'élément (un dto par exemple).
     */
    private async presentDetailActionSheet(layerType: LayerType, title: string, navigationExtras: NavigationExtras) {
        const actionSheet = await this.actionSheetController.create({
            header: title,
            buttons: [{
                text: 'Voir le detail',
                handler: () => {
                    this.openDetailPage(layerType, navigationExtras);
                }
            }, {
                text: 'Annuler',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    console.log('Cancel clicked');
                }
            }]
        });
        await actionSheet.present();
    }

    /**
     * Méthode permettant de récuper le type de la couche sur laquelle on a cliqué.
     * @param feature Element sur lequel on a cliqué.
     */
    private getLayerType(feature: any): LayerType {
        const id: string = feature.id;
        if (id.includes('arret_bus_layer')) {
            return LayerType.Bus;
        } else {
            if (id.includes('arret_tram_layer')) {
                return LayerType.Tramway;
            } else {
                if (id.includes('station_velo_layer')) {
                    return LayerType.StationVelo;
                } else {
                    if (id.includes('stationnement_velo_layer')) {
                        return LayerType.StationnementVelo;
                    }
                }
            }
        }
        throw new TypeError('LayerType inconnu !');
    }

    /**
     * Fonction permettant de créer un ArretTaoDto à l'aide du json et de présenter un action sheet de détail.
     * @param properties Json qui contient les infos de l'arrêt.
     * @param typeLayer Enumération permettant de savoir s'il s'agit d'un arrêt de bus ou d'un arrêt de tram.
     */
    private showArretTaoDetail(properties: any, typeLayer: LayerType) {
        // On conertie le json en ArretTaoDto
        const arret: ArretTaoDto = {
            gid: properties.gid,
            stopId: properties.stop_id,
            stopname: properties.stopname,
            parentSta: properties.parent_sta
        };
        const routeIdArret: string = this.getRouteId((arret.parentSta));
        // On vérifie que la routeId n'est pas vide
        if (routeIdArret !== '') {
            console.log(routeIdArret);
            let arretType: string;
            if (typeLayer === LayerType.Bus) {
                arretType = 'Arrêt de bus ';
            } else {
                arretType = 'Arrêt de tramway ';
            }
            const title = arretType + arret.stopname + ' | ' + 'Ligne ' + routeIdArret.substring(3, routeIdArret.length);
            const navigationExtras: NavigationExtras = {
                state: {
                    arretTao: arret,
                    routeId: routeIdArret,
                    layerType: typeLayer
                }
            };
            this.presentDetailActionSheet(typeLayer, title, navigationExtras).then();
        }
    }

    private showDetail(parsedJson: any) {
        // Par défaut on récupère le premier point
        const feature: any = HomePage.getFirstElement(parsedJson);
        try {
            // On récupère le type de couche sur lequel on a cliqué
            const layerType: LayerType = this.getLayerType(feature);
            switch (layerType) {
                case LayerType.Bus:
                case LayerType.Tramway:
                    this.showArretTaoDetail(feature.properties, layerType);
                    break;
                case LayerType.StationVelo:
                    this.showStationVeloDetail(feature.properties);
                    break;
                case LayerType.StationnementVelo:
                    this.showStationnementVeloDetail(feature.properties);
                    break;
                case LayerType.PisteCyclable:
                    break;
                default:
                    break;
            }
        } catch (e) {
        }
    }

    /**
     * Fonction permettant d'afficher ou de masquer une couche.
     * @param layerType Le type de couche à afficher ou à masquer.
     * @param visible Booléen à vrai si on veut afficher la couche, à faux sinon.
     */
    private showLayer(layerType: LayerType, visible: boolean) {
        console.log(this.layers);
        const layer: TileLayer = this.layers.get(layerType);
        if (layer !== undefined) {
            console.log(visible);
            layer.setVisible(visible);
        }
    }

    /**
     * Fonction permettant de créer un StationVeloDto à l'aide du json et de présenter un action sheet de détail.
     * @param properties Json qui contient les infos de la station vélo.
     */
    private showStationVeloDetail(properties: any) {
        console.log(properties);
        const station: StationVeloDto = {
            id: properties.id,
            nomstation: properties.nomstation,
            numvoie: properties.numvoie,
            voie: properties.voie,
            nomvoie: properties.nomvoie,
            codepostal: properties.codepostal,
            commune: properties.commune,
            numplace: properties.numplace
        };
        const navigationExtras: NavigationExtras = {
            state: {
                stationVelo: station
            }
        };
        this.presentDetailActionSheet(LayerType.StationVelo, 'Station Vélo\'+ ' + station.nomstation, navigationExtras).then();
    }


    /**
     * Fonction permettant de créer un StationnementVeloSto à l'aide du json et de présenter un action sheet de détail.
     * @param properties Json qui contient les infos du stationnement vélo.
     */
    private showStationnementVeloDetail(properties: any) {
        const stationnement: StationnementVeloDto = {
            numero: properties.numero,
            commune: properties.commune,
            insee: properties.insee,
            quartier: properties.quartier,
            rue: properties.rue,
            anneeReal: properties.annee_real,
            lieu: properties.lieu,
            type: properties.type,
            nbPlaces: properties.nb_places,
            couverture: properties.couverture,
            gardien: properties.gardien,
            panneau: properties.panneau,
            info: properties.info,
            maj: properties.maj
        };
        const navigationExtras: NavigationExtras = {
            state: {
                stationnementVelo: stationnement
            }
        };
        this.presentDetailActionSheet(LayerType.StationnementVelo,
            stationnement.type + ' | ' + stationnement.commune,
            navigationExtras).then();
    }

    async getPosition() {
        const pos = this.positionService.currentPosition();
        await pos.then(resp => {
            this.latitude = resp.coords.latitude;
            this.longitude = resp.coords.longitude;
        });
    }

    /**
     * Fonction permettant d'afficher la page recherche des pistes cyclables.
     */
    async presentPisteListModal() {
        const modal = await this.modalController.create({
            component: ModalPistesCyclablesListPage
        });
        await modal.present();
    }

}
