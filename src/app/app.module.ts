import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {ModalAddRetardPageModule} from './modals/modal-add-retard/modal-add-retard.module';
import {ModalRetardListPageModule} from './modals/modal-retard-list/modal-retard-list.module';
import {ModalAddCoupureLignePage} from './modals/modal-add-coupure-ligne/modal-add-coupure-ligne.page';
import {ModalAddCoupureLignePageModule} from './modals/modal-add-coupure-ligne/modal-add-coupure-ligne.module';
import {ModalCoupureListPage} from './modals/modal-coupure-list/modal-coupure-list.page';
import {ModalCoupureListPageModule} from './modals/modal-coupure-list/modal-coupure-list.module';
import {ModalAddDegradationPageModule} from './modals/modal-add-degradation/modal-add-degradation.module';
import {ModalDegradationListPage} from './modals/modal-degradation-list/modal-degradation-list.page';
import {ModalDegradationListPageModule} from './modals/modal-degradation-list/modal-degradation-list.module';
import {ModalAddPenuriePage} from './modals/modal-add-penurie/modal-add-penurie.page';
import {ModalAddPenuriePageModule} from './modals/modal-add-penurie/modal-add-penurie.module';
import {ModalPenurieListPageModule} from './modals/modal-penurie-list/modal-penurie-list.module';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ModalPistesCyclablesListPageModule } from './modals/modal-pistes-cyclables-list/modal-pistes-cyclables-list.module';
import { ModalPisteDetailPageModule } from './modals/modal-piste-detail/modal-piste-detail.module';


@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [BrowserModule, IonicModule.forRoot(),
        AppRoutingModule, HttpClientModule, FormsModule,
        ModalAddRetardPageModule, ModalRetardListPageModule,
        ModalAddCoupureLignePageModule, ModalCoupureListPageModule,
        ModalAddDegradationPageModule, ModalDegradationListPageModule,
        ModalAddPenuriePageModule, ModalPenurieListPageModule, ModalPistesCyclablesListPageModule,
        ModalPisteDetailPageModule],
    providers: [
        StatusBar,
        SplashScreen, Camera, Geolocation,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
