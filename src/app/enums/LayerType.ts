export enum LayerType {
    Bus,
    Tramway,
    StationVelo,
    StationnementVelo,
    PisteCyclable
}
