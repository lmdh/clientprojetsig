import {LayerType} from './enums/LayerType';
import {TileWMS} from 'ol/source';
import TileLayer from 'ol/layer/Tile';
import {transformExtent} from 'ol/proj';
import {RestApiService} from './services/rest-api.service';

export class LayerFactory {

    private readonly url = 'http://localhost:8000/geoserver/SIGProject/wms';
    private readonly serverType = 'geoserver';
    private restApiService: RestApiService;

    public getTileWMS(layer: string): TileWMS {
        return new TileWMS({
            url: this.url,
            params: {
                LAYERS: layer
            },
            serverType: this.serverType
        });
    }

    public getSource(layerType: LayerType): TileWMS {
        switch (layerType) {
            case LayerType.Bus:
                return this.getTileWMS('SIGProject:arret_bus_layer');
            case LayerType.Tramway:
                return this.getTileWMS('SIGProject:arret_tram_layer');
            case LayerType.StationVelo:
                return this.getTileWMS('SIGProject:station_velo_layer');
            case LayerType.StationnementVelo:
                return this.getTileWMS('SIGProject:stationnement_velo_layer');
            case LayerType.PisteCyclable:
                break;
            default:
                break;
        }
    }

    public getLayer(layerSource: TileWMS) {
        return new TileLayer({
            extent: transformExtent(
                [1.8, 47.8, 2.06, 47.97],
                'EPSG:4326',
                'EPSG:3857'
            ),
            source: layerSource
        });
    }
}
