export class StationnementVeloDto {
    numero: string;
    commune: string;
    insee: string;
    quartier: string;
    rue: string;
    anneeReal: number;
    lieu: string;
    type: string;
    nbPlaces: number;
    couverture: string;
    gardien: string;
    panneau: string;
    info: string;
    maj: string;
}
