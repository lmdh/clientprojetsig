export class ResponseDto<T> {
    data: T;
    success: boolean;
    errorCode: string;
    errorMessage: string;
}
