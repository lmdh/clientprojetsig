export class StationVeloDto {
    id: number;
    nomstation: string;
    numvoie: string;
    voie: string;
    nomvoie: string;
    codepostal: string;
    commune: string;
    numplace: string;
}
