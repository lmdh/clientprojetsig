export class AuthResultDto {

    accessToken: string;
    refreshToken: string;
    expiresIn: number;

    toString(): string {
        return 'accessToken ' + this.accessToken + '\n' + 'refreshToken ' + this.refreshToken + '\n' + 'expirationTime ' + this.expiresIn;
    }
}
