export class PisteCyclableDto {

    gid: number;
    numero: string;
    commune: string;
    quartier: string;
    rue: string;
    longueur: string;
    sensCycl: string;
    longCycl: string;
    typeIti: string;
    position: string;
    largeur: string;
    revetement: string;
    codcommune: string;
    geom: number[][];
    note: number;
}
