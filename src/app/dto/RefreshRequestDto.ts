export class RefreshRequestDto {
    refreshToken: string;


    constructor(refreshToken: string) {
        this.refreshToken = refreshToken;
    }
}
