export class CoupureLigneDto {

    message: string;
    idArretDepart: number;
    idArretArrivee: number;
    date: Date;
}
