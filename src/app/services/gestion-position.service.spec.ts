import { TestBed } from '@angular/core/testing';

import { GestionPositionService } from './gestion-position.service';

describe('GestionPositionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GestionPositionService = TestBed.get(GestionPositionService);
    expect(service).toBeTruthy();
  });
});
