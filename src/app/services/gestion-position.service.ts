import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';

/*
  Service permettant de récupérer la position de l'utilisateur
 */
@Injectable({
  providedIn: 'root'
})
export class GestionPositionService {

  constructor(private geolocation: Geolocation) { }

  /*
    Méthode renvoyant la position de l'utilisateur
   */
  public currentPosition() {
    return this.geolocation.getCurrentPosition();
  }
}
