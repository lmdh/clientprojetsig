import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {AuthResultDto} from '../dto/AuthResultDto';
import {Observable, throwError} from 'rxjs';
import {AuthRequestDto} from '../dto/AuthRequestDto';
import {catchError} from 'rxjs/operators';
import {RetardDto} from '../dto/signalement/RetardDto';
import {ArretTaoDto} from '../dto/ArretTaoDto';
import {CoupureLigneDto} from '../dto/signalement/CoupureLigneDto';
import {DegradationDto} from '../dto/signalement/degradation/DegradationDto';
import {PenurieDto} from '../dto/signalement/PenurieDto';
import { PisteCyclableDto } from '../dto/PisteCyclableDto';
import { NoteDto } from '../dto/NoteDto';
import {RefreshRequestDto} from '../dto/RefreshRequestDto';

@Injectable({
    providedIn: 'root'
})
export class RestApiService {

    private readonly apiUrl = 'http://localhost:8080';
    private readonly authUrl = '/user';
    private readonly signalementUrl = '/signalement';
    public accessToken: string;
    public refreshToken: string;
    public expiresIn: number;
    public tokenTime: Date;

    constructor(private http: HttpClient) {
    }

    /**
     * Méthode permettant de récupérer un header contenant l'access token
     */
    public getHttpOptions() {
        return {
            headers: new HttpHeaders({
                Authorization: this.accessToken
            })
        };
    }

    public setTokens(authResult: AuthResultDto) {
        this.accessToken = authResult.accessToken;
        this.refreshToken = authResult.refreshToken;
        this.expiresIn = authResult.expiresIn;
        this.tokenTime = new Date();
    }

    /**
     * Fonction permettant de vérifier que l'access token actuel est toujours valide. Et si il ne l'ai plus, de le mettre à jour.
     */
    async checkToken() {
        const expTime = new Date(this.tokenTime.getTime() + this.expiresIn);
        const now = new Date();
        if (now > expTime) {
            const promise = await this.http.post<AuthResultDto>(this.apiUrl + this.authUrl + '/refresh',
                new RefreshRequestDto(this.refreshToken))
                .pipe(catchError(this.handleError)).toPromise();
            this.setTokens(promise);
        }
    }


    /**
     * Fonction permettant de gérer les erreurs http envoyées par le webservice
     * @param error L'erreur http.
     */
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(
                `Backend returned code ${error.status}, ` +
                `message : ${error.message} ` +
                `body was: ${error.error}`);
        }
        return throwError(
            error.error);
    }

    public postLogin(authRequest: AuthRequestDto): Observable<AuthResultDto> {
        return this.http.post<AuthResultDto>(this.apiUrl + this.authUrl + '/login', authRequest)
            .pipe(catchError(this.handleError));
    }

    public postRegister(authRequest: AuthRequestDto): Observable<AuthResultDto> {
        return this.http.post<AuthResultDto>(this.apiUrl + this.authUrl + '/register', authRequest)
            .pipe(catchError(this.handleError));
    }

    public postRetard(retardDto: RetardDto, routeId: string) {
        return this.http.post(this.apiUrl + this.signalementUrl + '/retard/' + routeId, retardDto, this.getHttpOptions())
            .pipe(catchError(this.handleError));
    }

    public getRetard(routeId: string, arretId: number): Observable<RetardDto[]> {
        return this.http.get<RetardDto[]>(this.apiUrl + this.signalementUrl + '/retard/' + routeId + '/' + arretId)
            .pipe(catchError(this.handleError));
    }

    public getArrets(routeId: string): Observable<ArretTaoDto[]> {
      return this.http.get<ArretTaoDto[]>(this.apiUrl + this.signalementUrl + '/' + routeId + '/arrets')
          .pipe(catchError(this.handleError));
    }

    public postCoupureLigne(coupureLigneDto: CoupureLigneDto, routeId: string) {
        return this.http.post(this.apiUrl + this.signalementUrl + '/coupure/' + routeId, coupureLigneDto, this.getHttpOptions())
            .pipe(catchError(this.handleError));
    }

    public getCoupures(routeId: string): Observable<CoupureLigneDto[]> {
        return this.http.get<CoupureLigneDto[]>(this.apiUrl + this.signalementUrl + '/coupure/' + routeId)
            .pipe(catchError(this.handleError));
    }

    public postDegradationArretTao(degradation: DegradationDto, arretId: number) {
        return this.http.post(this.apiUrl + this.signalementUrl + '/degradation/arretTao/' + arretId, degradation, this.getHttpOptions())
            .pipe(catchError(this.handleError));
    }

    public getDegradationArretTao(arretId: number): Observable<DegradationDto[]> {
        return this.http.get<DegradationDto[]>(this.apiUrl + this.signalementUrl + '/degradation/arretTao/' + arretId)
            .pipe(catchError(this.handleError));
    }

    public postDegradationStationVelo(degradation: DegradationDto, arretId: number) {
        return this.http.post(this.apiUrl + this.signalementUrl + '/degradation/stationVelo/' + arretId, degradation, this.getHttpOptions())
            .pipe(catchError(this.handleError));
    }

    public getDegradationStationVelo(stationId: number): Observable<DegradationDto[]> {
        return this.http.get<DegradationDto[]>(this.apiUrl + this.signalementUrl + '/degradation/stationVelo/' + stationId)
            .pipe(catchError(this.handleError));
    }

    public postDegradationStationnementVelo(degradation: DegradationDto, numero: string) {
        return this.http.post(this.apiUrl + this.signalementUrl + '/degradation/stationnementVelo/' + numero, degradation,
            this.getHttpOptions())
            .pipe(catchError(this.handleError));
    }

    public getDegradationStationnementVelo(numero: string): Observable<DegradationDto[]> {
        return this.http.get<DegradationDto[]>(this.apiUrl + this.signalementUrl + '/degradation/stationnementVelo/' + numero)
            .pipe(catchError(this.handleError));
    }

    public postPenurie(penurie: PenurieDto, id: number) {
        return this.http.post(this.apiUrl + this.signalementUrl + '/penurie/' + id, penurie, this.getHttpOptions())
            .pipe(catchError(this.handleError));
    }

    public getPenuries(id: number) {
        return this.http.get<PenurieDto[]>(this.apiUrl + this.signalementUrl + '/penurie/' + id)
            .pipe(catchError(this.handleError));
    }

    public getPiste(pisteId: number): Observable<PisteCyclableDto> {
        return this.http.get<PisteCyclableDto>(this.apiUrl + '/piste/' + pisteId)
            .pipe(catchError(this.handleError));
    }
    public getAllPistes(): Observable<PisteCyclableDto[]> {
        return this.http.get<PisteCyclableDto[]>(this.apiUrl + '/piste/all')
            .pipe(catchError(this.handleError));
    }
    public getPisteGeom(pisteId: number): Observable<PisteCyclableDto> {
        return this.http.get<PisteCyclableDto>(this.apiUrl + '/piste/' + pisteId + '/geom')
            .pipe(catchError(this.handleError));
    }
    public getNote(pisteId: number): Observable<NoteDto> {
        return this.http.get<NoteDto>(this.apiUrl + '/notes/' + pisteId)
            .pipe(catchError(this.handleError));
    }
    public postNote(pisteId: number, noteDto: NoteDto) {
        return this.http.post(this.apiUrl + '/notes/' + pisteId, noteDto, this.getHttpOptions())
            .pipe(catchError(this.handleError));
    }
}
