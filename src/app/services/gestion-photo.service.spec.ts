import { TestBed } from '@angular/core/testing';

import { GestionPhotoService } from './gestion-photo.service';

describe('GestionPhotoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GestionPhotoService = TestBed.get(GestionPhotoService);
    expect(service).toBeTruthy();
  });
});
