import {Injectable} from '@angular/core';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';

/*
    Service gérant la prise de photo
 */
@Injectable({
  providedIn: 'root'
})
export class GestionPhotoService {

   private optionsCamera: CameraOptions = {
        quality: 100,
        sourceType: this.camera.PictureSourceType.CAMERA,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
  };

   private optionsGallery: CameraOptions = {
        quality: 100,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
  };

   constructor(private camera: Camera) {
  }

  /*
    Méthode utilisant optionsCamera pour prendre une photo
   */
  public async takePicture() {
      return await this.camera.getPicture(this.optionsCamera);
  }

  /*
    Méthode utilisant optionsGallery pour choisir une photo depuis la gallerie
   */
  public async choosePictureFromGallery() {
    return await this.camera.getPicture(this.optionsGallery);
  }
}
