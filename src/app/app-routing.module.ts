import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'connection', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)},
  { path: 'connection', loadChildren: './pages/connection/connection.module#ConnectionPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'arret-tao', loadChildren: './pages/arret-tao/arret-tao.module#ArretTaoPageModule' },
  { path: 'station-velo', loadChildren: './pages/station-velo/station-velo.module#StationVeloPageModule' },
  { path: 'modal-add-penurie', loadChildren: './modals/modal-add-penurie/modal-add-penurie.module#ModalAddPenuriePageModule' },
  { path: 'modal-penurie-list', loadChildren: './modals/modal-penurie-list/modal-penurie-list.module#ModalPenurieListPageModule' },
  { path: 'stationnement-velo', loadChildren: './pages/stationnement-velo/stationnement-velo.module#StationnementVeloPageModule' },
  { path: 'modal-pistes-cyclables-list', loadChildren: './modals/modal-pistes-cyclables-list/modal-pistes-cyclables-list.module#ModalPistesCyclablesListPageModule' },
  { path: 'modal-piste-detail', loadChildren: './modals/modal-piste-detail/modal-piste-detail.module#ModalPisteDetailPageModule' },




];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
