import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertController, ModalController} from '@ionic/angular';
import {RestApiService} from '../../services/rest-api.service';
import {LayerType} from '../../enums/LayerType';
import {RetardDto} from '../../dto/signalement/RetardDto';
import {DegradationDto} from '../../dto/signalement/degradation/DegradationDto';
import {GestionPhotoService} from '../../services/gestion-photo.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-modal-add-degradation',
    templateUrl: './modal-add-degradation.page.html',
    styleUrls: ['./modal-add-degradation.page.scss'],
})
export class ModalAddDegradationPage implements OnInit, OnDestroy {

    @Input() id: any;
    @Input() degradationType: LayerType;
    @Input() title: string;
    subscription: Subscription;
    addDegradationForm: FormGroup;
    photo: string;

    constructor(public formBuilder: FormBuilder,
                private modalCtrl: ModalController,
                private restApiService: RestApiService,
                private photoService: GestionPhotoService,
                private alertController: AlertController) {
        this.addDegradationForm = this.formBuilder.group({
            message: ['', Validators.required]
        });
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        if (this.subscription != null) {
            this.subscription.unsubscribe();
        }
    }

    /**
     * Fonction permettant d'ajouter une dégradation.
     */
    async addDegradation() {
        const degradationDto: DegradationDto = new DegradationDto();
        degradationDto.message = this.addDegradationForm.get('message').value;
        degradationDto.date = new Date();
        degradationDto.photo = this.photo;
        await this.restApiService.checkToken();
        // On appelle une méthode du web service suivant le type de l'élément sur lequel on déclare une dégradation
        switch (this.degradationType) {
            case LayerType.Bus:
            case LayerType.Tramway:
                console.log(this.id);
                this.subscription = this.restApiService.postDegradationArretTao(degradationDto, this.id).subscribe(() => {
                    this.addDegrationResult();
                }, error => {
                    this.addDegradationError(error);
                });
                break;
            case LayerType.StationnementVelo:
                this.subscription = this.restApiService.postDegradationStationnementVelo(degradationDto, this.id).subscribe(() => {
                    this.addDegrationResult();
                }, error => {
                    this.addDegradationError(error);
                });
                break;
            case LayerType.StationVelo:
                this.subscription = this.restApiService.postDegradationStationVelo(degradationDto, this.id).subscribe(() => {
                    this.addDegrationResult();
                }, error => {
                    this.addDegradationError(error);
                });
                break;
            default:
                break;
        }
    }

    /*
        Méthode appellant le service permettant de prendre des photos
     */
    async getPhoto() {
        this.photo = await this.photoService.takePicture();
    }

    /**
     * Fonction permettant d'afficher l'alert de confirmation d'ajout de dégradation et de revenir vers la page de detail.
     */
    private addDegrationResult() {
        this.modalCtrl.dismiss().then(() => {
                this.alertController.create({
                    header: 'Succès',
                    message: 'Signalement réussi !',
                    buttons: ['Ok']
                }).then(alert => alert.present());
            }
        );
    }

    /** Permet d'afficher une alert box contenant le message d'erreur passé en paramètre.
     * @param error Le message d'erreur à afficher.
     */
    private addDegradationError(error: any) {
        this.alertController.create({
            header: 'Echec',
            message: error,
            buttons: ['Ok']
        }).then(alert => alert.present());
    }

    /**
     * Fonction permettant de fermer la page actuelle.
     */
    dismissModal() {
        this.modalCtrl.dismiss();
    }
}
