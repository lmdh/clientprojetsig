import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ModalAddCoupureLignePage } from './modal-add-coupure-ligne.page';

const routes: Routes = [
  {
    path: '',
    component: ModalAddCoupureLignePage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule
    ],
  declarations: [ModalAddCoupureLignePage]
})
export class ModalAddCoupureLignePageModule {}
