import {Component, Input, OnInit} from '@angular/core';
import {RestApiService} from '../../services/rest-api.service';
import {ArretTaoDto} from '../../dto/ArretTaoDto';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CoupureLigneDto} from '../../dto/signalement/CoupureLigneDto';
import {AlertController, ModalController} from '@ionic/angular';

@Component({
    selector: 'app-modal-add-coupure-ligne',
    templateUrl: './modal-add-coupure-ligne.page.html',
    styleUrls: ['./modal-add-coupure-ligne.page.scss'],
})
export class ModalAddCoupureLignePage implements OnInit {

    constructor(public formBuilder: FormBuilder,
                private modalCtrl: ModalController,
                private restApiService: RestApiService,
                private alertController: AlertController) {
        // On définit les champs du formulaire et on indique si ils sont requis.
        this.addCoupureForm = this.formBuilder.group({
            arretDepart: ['', Validators.required],
            arretArrivee: ['', Validators.required],
            message: ['', Validators.required]
        }, {validators: ModalAddCoupureLignePage.checkArrets});
    }

    addCoupureForm: FormGroup;
    @Input() routeId: string;
    private arrets: ArretTaoDto[];
    public routeName: string;

    errorMessages = {
        arretArrivee: [
            {type: 'same', message: 'Les deux arrêts doivent être différents !'}
        ]
    };

    /**
     * Méthode de validation de formulaire qui va permettre de vérifier que l'arrêt de départ et l'arrêt d'arrivée sont différents.
     * @param group Le formulaire qui va être validé.
     */
    static checkArrets(group: FormGroup) {
        const arretArrivee = group.get('arretArrivee');
        const arretDepart = group.get('arretDepart');
        if (arretArrivee.value === arretDepart.value) {
            arretArrivee.setErrors({same: true});
        }
        return null;
    }

    ngOnInit() {
        console.log(this.routeId);
        this.getArrets();
        this.routeName = this.routeId.substring(3, this.routeId.length);
    }

    /**
     * Fonction permettant de récupérer les arrêts de la ligne actuelle.
     */
    private getArrets() {
        this.restApiService.getArrets(this.routeId).subscribe(arrets => this.arrets = arrets);
    }

    /**
     * Fonction permettant d'afficher l'alert de confirmation d'ajout de coupure de ligne et de revenir vers la page de detail de l'arrêt.
     */
    private addCoupureLigneResult() {
        this.modalCtrl.dismiss().then(() => {
                this.alertController.create({
                    header: 'Succès',
                    message: 'Signalement réussi !',
                    buttons: ['Ok']
                }).then(alert => alert.present());
            }
        );
    }

    /** Permet d'afficher une alert box contenant le message d'erreur passé en paramètre.
     * @param error Le message d'erreur à afficher.
     */
    private addCoupureLigneError(error: any) {
        this.alertController.create({
            header: 'Echec',
            message: error,
            buttons: ['Ok']
        }).then(alert => alert.present());
    }

    /**
     * Fonction permettant d'ajouter une coupure de ligne.
     */
    public async addCoupure() {
        const coupure: CoupureLigneDto = {
            message: this.addCoupureForm.get('message').value,
            idArretArrivee: this.addCoupureForm.get('arretArrivee').value,
            idArretDepart: this.addCoupureForm.get('arretDepart').value,
            date: new Date()
        };
        await this.restApiService.checkToken();
        this.restApiService.postCoupureLigne(coupure, this.routeId).subscribe(
            () => this.addCoupureLigneResult(),
            error => this.addCoupureLigneError(error));

    }

    /**
     * Fonction permettant de fermer la page actuelle.
     */
    dismissModal() {
        this.modalCtrl.dismiss();
    }
}
