import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDegradationListPage } from './modal-degradation-list.page';

describe('ModalDegradationListPage', () => {
  let component: ModalDegradationListPage;
  let fixture: ComponentFixture<ModalDegradationListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDegradationListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDegradationListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
