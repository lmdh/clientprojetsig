import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {LayerType} from '../../enums/LayerType';
import {ModalController} from '@ionic/angular';
import {RestApiService} from '../../services/rest-api.service';
import {DomSanitizer} from '@angular/platform-browser';
import {DegradationDto} from '../../dto/signalement/degradation/DegradationDto';
import {takeUntil} from 'rxjs/operators';
import {Subscription} from 'rxjs';


@Component({
    selector: 'app-modal-degradation-list',
    templateUrl: './modal-degradation-list.page.html',
    styleUrls: ['./modal-degradation-list.page.scss'],
})
export class ModalDegradationListPage implements OnInit, OnDestroy {

    @Input() id: any;
    @Input() degradationType: LayerType;
    @Input() title: string;
    public degradations: DegradationDto[];
    subscription: Subscription;

    constructor(
        private modalCtrl: ModalController,
        private restApiService: RestApiService,
        private domSanitizer: DomSanitizer
    ) {
    }

    ngOnInit() {
        this.getDegradations(this.degradationType);
    }

    ngOnDestroy() {
        if (this.subscription != null) {
            this.subscription.unsubscribe();
        }
    }

  /**
   * Fonction permettant de récupérer les dégradations.
   * @param degradationType Type de l'élément sur lequel la dégradation a été signalée.
   */
  private getDegradations(degradationType: LayerType) {
        switch (degradationType) {
            case LayerType.Bus:
            case LayerType.Tramway:
                this.subscription = this.restApiService.getDegradationArretTao(this.id)
                    .subscribe(degradations => this.degradations = degradations);
                console.log(this.degradations);
                break;
            case LayerType.StationVelo:
                this.subscription = this.subscription = this.restApiService.getDegradationStationVelo(this.id)
                    .subscribe(degradations => this.degradations = degradations);
                break;
            case LayerType.StationnementVelo:
                this.subscription = this.subscription = this.restApiService.getDegradationStationnementVelo(this.id)
                    .subscribe(degradations => this.degradations = degradations);
                break;
            case LayerType.PisteCyclable:
                break;
        }
    }

  /*
    Méthode permettant d'afficher une image venant d'une URL en bypassant les contrôle de sécurité. L'image venant de la base de donnée de
    l'application, on suppose qu'elle est digne de confiance
   */
  sanitarize(imageUrl: string) {
    return this.domSanitizer.bypassSecurityTrustUrl(imageUrl);
  }

  /**
   * Fonction permettant de fermer la page actuelle.
   */
    dismissModal() {
        this.modalCtrl.dismiss();
    }


}
