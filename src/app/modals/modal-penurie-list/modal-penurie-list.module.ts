import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ModalPenurieListPage } from './modal-penurie-list.page';

const routes: Routes = [
  {
    path: '',
    component: ModalPenurieListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ModalPenurieListPage]
})
export class ModalPenurieListPageModule {}
