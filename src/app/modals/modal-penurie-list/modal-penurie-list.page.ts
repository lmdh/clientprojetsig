import {Component, Input, OnInit} from '@angular/core';
import {PenurieDto} from '../../dto/signalement/PenurieDto';
import {AlertController, ModalController} from '@ionic/angular';
import {RestApiService} from '../../services/rest-api.service';
import {StationVeloDto} from '../../dto/StationVeloDto';

@Component({
    selector: 'app-modal-penurie-list',
    templateUrl: './modal-penurie-list.page.html',
    styleUrls: ['./modal-penurie-list.page.scss'],
})
export class ModalPenurieListPage implements OnInit {

    @Input() station: StationVeloDto;
    public penuries: PenurieDto[];
    public title: string;

    constructor(private modalCtrl: ModalController,
                private restApiService: RestApiService) {
    }

    ngOnInit() {
        this.getPenuries();
        this.title = this.station.nomstation;
    }

    /**
     * Fonction permettant de récuperer les pénuries d'une station vélo +.
     */
    private getPenuries() {
        this.restApiService.getPenuries(this.station.id).subscribe(penuries => this.penuries = penuries);
    }

    /**
     * Fonction permettant de fermer la page actuelle.
     */
    public dismissModal() {
        this.modalCtrl.dismiss();
    }
}
