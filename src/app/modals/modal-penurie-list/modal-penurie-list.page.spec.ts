import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPenurieListPage } from './modal-penurie-list.page';

describe('ModalPenurieListPage', () => {
  let component: ModalPenurieListPage;
  let fixture: ComponentFixture<ModalPenurieListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPenurieListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPenurieListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
