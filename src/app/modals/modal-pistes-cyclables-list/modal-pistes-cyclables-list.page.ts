import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { PisteCyclableDto } from 'src/app/dto/PisteCyclableDto';
import { RestApiService } from 'src/app/services/rest-api.service';
import { ModalPisteDetailPage } from '../modal-piste-detail/modal-piste-detail.page';

@Component({
  selector: 'app-modal-pistes-cyclables-list',
  templateUrl: './modal-pistes-cyclables-list.page.html',
  styleUrls: ['./modal-pistes-cyclables-list.page.scss'],
})

export class ModalPistesCyclablesListPage implements OnInit {

  public pistes: PisteCyclableDto[];
  public pistesList: PisteCyclableDto[];
  public searchTerm: string;
  constructor(private modalCtrl: ModalController, private apiService: RestApiService, private navCntrl: NavController) {
  }
  private getData() {
    const subscription = this.apiService.getAllPistes().subscribe(
      data => {
        this.pistesList = data;
        this.pistes = this.pistesList;
        this.sortByKey(this.pistesList, 'rue');
      }
    );
  }


  ngOnInit() {
    this.getData();
  }

  async details(id) {
    const detailsModal = await this.modalCtrl.create({
      component: ModalPisteDetailPage,
      componentProps: {
        piste: this.getSelectedPiste(id)
      }
    });
    detailsModal.present();
  }

  getSelectedPiste(id) {
    for (let index = 0; index < this.pistes.length; index++) {
      if (this.pistes[index].gid === id ) {
        return this.pistes[index];
      }
    }
  }

  filterItems(event) {
    const searchTerm = event.target.value;
    if (searchTerm === '') { this.pistesList = this.sortByKey(this.pistes, 'rue'); } else {
      this.pistesList = this.pistesList.filter((piste) => {
        if (piste.rue !== null) {
          if (piste.rue.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
            return piste;
          }
        }
      });
    }
  }
  public dismissModal() {
    this.modalCtrl.dismiss();
  }
  public sortByKey(array, key) {
    return array.sort((a: PisteCyclableDto, b: PisteCyclableDto) => {
      const x = a[key];
      const y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 0 : 1));
    });
  }
}
