import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPistesCyclablesListPage } from './modal-pistes-cyclables-list.page';

describe('ModalPistesCyclablesListPage', () => {
  let component: ModalPistesCyclablesListPage;
  let fixture: ComponentFixture<ModalPistesCyclablesListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalPistesCyclablesListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPistesCyclablesListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
