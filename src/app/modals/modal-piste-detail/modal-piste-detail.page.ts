import {Component, OnInit, Input} from '@angular/core';
import {PisteCyclableDto} from 'src/app/dto/PisteCyclableDto';
import {RestApiService} from 'src/app/services/rest-api.service';
import {ModalController, AlertController} from '@ionic/angular';
import {NoteDto} from 'src/app/dto/NoteDto';
import TileLayer from 'ol/layer/Tile';
import {OSM} from 'ol/source';
import OlMap from 'ol/Map';
import VectorSource from 'ol/source/Vector';
import {Feature} from 'ol';
import {Style} from 'ol/style';
import Stroke from 'ol/style/Stroke';
import VectorLayer from 'ol/layer/Vector';
import {View} from 'ol';
import {fromLonLat} from 'ol/proj';
import {Coordinate} from 'ol/coordinate';
import LineString from 'ol/geom/LineString';
import Icon from 'ol/style/Icon';

@Component({
    selector: 'app-modal-piste-detail',
    templateUrl: './modal-piste-detail.page.html',
    styleUrls: ['./modal-piste-detail.page.scss'],
})


export class ModalPisteDetailPage implements OnInit {

    private map: OlMap;
    private view: View;
    private pisteLayer: VectorLayer;
    @Input() piste: PisteCyclableDto;

    constructor(private restApiService: RestApiService,
                private alertCtrl: AlertController, private modalController: ModalController) {
    }

    drawMap() {
        const osm = new TileLayer({
            source: new OSM({
                opaque: false
            })
        });


        const line = new LineString(this.piste.geom);
        const centerPoint = line.getCoordinateAt(0.5);
        this.view = new View({
            center: fromLonLat(centerPoint),
            zoom: 15
        });
        const feature = new Feature({
            name: 'Piste Cyclable' + this.piste.rue,
            geometry: line
        });
        /*const iconStyle = new Style({
            image: new Icon(({
                anchor: [0.5, 1],
                src: 'http://cdn.mapmarker.io/api/v1/pin?text=P&size=50&hoffset=1'
            }))
        });*/

        line.transform('EPSG:4326', 'EPSG:3857');
        const lineStyle = new Style({
            stroke: new Stroke({
                color: '#52E116',
                width: 5
            })
        });
        const source = new VectorSource({
            features: [feature]
        });
        this.pisteLayer = new VectorLayer({
            source,
            style: [lineStyle]
        });
        this.map = new OlMap({
            target: 'map1',
            layers: [osm, this.pisteLayer],
            view: this.view
        });
    }

    ngOnInit() {
        this.piste.longueur = Number(this.piste.longueur).toFixed(2);
        this.piste.largeur = Number(this.piste.longueur).toFixed(2);
        setTimeout(() => this.drawMap(), 1000);
    }

    dismissModal() {
        this.modalController.dismiss();
    }

    async showRatingAlert() {
        const alert = await this.alertCtrl.create({
            header: 'Note',
            cssClass: 'alertstar',
            buttons: [
                {
                    text: '1', handler: data => {
                        this.noter(1);
                    }
                },
                {
                    text: '2', handler: data => {
                        this.noter(2);
                    }
                },
                {
                    text: '3', handler: data => {
                        this.noter(3);
                    }
                },
                {
                    text: '4', handler: data => {
                        this.noter(4);
                    }
                },
                {
                    text: '5', handler: data => {
                        this.noter(5);
                    }
                }
            ]
        });

        alert.present();
    }

    noter(note) {
        const noteDto: NoteDto = new NoteDto();
        noteDto.note = note;
        this.restApiService.postNote(this.piste.gid, noteDto).subscribe(() => {
            this.getNewNote(this.piste.gid);
        }, error => {
            console.log('erreure dans l\'insertion de la note');
        });
    }

    async updateNote(value: number) {
        this.piste.note = value;
    }

    async getNewNote(id) {
        let noteDto: NoteDto;
        return new Promise(resolve => {
            this.restApiService.getNote(this.piste.gid).subscribe(
                (data) => {
                    noteDto = data,
                        this.updateNote(noteDto.note);
                });
        });
    }

    getCoordinates(list: number[][]) {
        let coordinates: Coordinate[];
        coordinates = [];
        for (let index = 0; index < list.length; index++) {
            coordinates[index] = list[index]; // [list[index][0],list[index][1]];
        }
        return coordinates;
    }
}
