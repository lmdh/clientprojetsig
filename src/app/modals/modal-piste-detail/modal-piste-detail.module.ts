import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ModalPisteDetailPage } from './modal-piste-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ModalPisteDetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ModalPisteDetailPage]
})
export class ModalPisteDetailPageModule {}
