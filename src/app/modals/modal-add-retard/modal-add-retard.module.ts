import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ModalAddRetardPage } from './modal-add-retard.page';
import {ArretTaoPage} from '../../pages/arret-tao/arret-tao.page';

const routes: Routes = [
  {
    path: '',
    component: ModalAddRetardPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule
    ],
  declarations: [ModalAddRetardPage],
})
export class ModalAddRetardPageModule {}
