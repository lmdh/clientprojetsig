import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AlertController, ModalController, NavParams} from '@ionic/angular';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthResultDto} from '../../dto/AuthResultDto';
import {AuthRequestDto} from '../../dto/AuthRequestDto';
import {RestApiService} from '../../services/rest-api.service';
import {RetardDto} from '../../dto/signalement/RetardDto';

@Component({
    selector: 'app-modal-add-retard',
    templateUrl: './modal-add-retard.page.html',
    styleUrls: ['./modal-add-retard.page.scss'],
})
export class ModalAddRetardPage implements OnInit{

    @Input() gid: number;
    @Input() routeId: string;
    @Input() arretType: string;
    @Input() stopname: string;
    addRetardForm: FormGroup;
    public routeName: string;

    ngOnInit(): void {
        this.routeName = this.routeId.substring(3, this.routeId.length);
    }


    constructor(
        public formBuilder: FormBuilder,
        private modalCtrl: ModalController,
        private restApiService: RestApiService,
        private alertController: AlertController) {
        // On définit les champs du formulaire et on indique si ils sont requis.
        this.addRetardForm = this.formBuilder.group({
            retardTime: new FormControl('', {validators: [Validators.required, Validators.min(1)]}),
            message: ['', Validators.required]
        });
    }

    /**
     * Fonction permettant d'afficher l'alert de confirmation d'ajout du retard et de revenir vers la page de detail de l'arrêt.
     */
    private addRetardResult() {
        this.modalCtrl.dismiss().then(() => {
                this.alertController.create({
                    header: 'Succès',
                    message: 'Signalement réussi !',
                    buttons: ['Ok']
                }).then(alert => alert.present());
            }
        );
    }

    /** Permet d'afficher une alert box contenant le message d'erreur passé en paramètre.
     * @param error Le message d'erreur à afficher.
     */
    private addRetardError(error: any) {
        this.alertController.create({
            header: 'Echec',
            message: error,
            buttons: ['Ok']
        }).then(alert => alert.present());
    }


    /**
     * Fonction permettant d'ajouter un retard
     */
    public async addRetard() {
        const retardDto: RetardDto = new RetardDto();
        retardDto.duree = this.addRetardForm.get('retardTime').value;
        retardDto.message = this.addRetardForm.get('message').value;
        retardDto.date = new Date();
        retardDto.idArret = this.gid;
        await this.restApiService.checkToken();
        this.restApiService.postRetard(retardDto, this.routeId).subscribe(() => {
            this.addRetardResult();
        }, error => {
            this.addRetardError(error);
        });
    }

    /**
     * Fonction permettant de fermer la page actuelle.
     */
    dismissModal() {
        this.modalCtrl.dismiss();
    }
}
