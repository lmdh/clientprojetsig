import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {RetardDto} from '../../dto/signalement/RetardDto';
import {RestApiService} from '../../services/rest-api.service';
import {ModalController} from '@ionic/angular';
import {DatePipe} from '@angular/common';


@Component({
    selector: 'app-modal-retard-list',
    templateUrl: './modal-retard-list.page.html',
    styleUrls: ['./modal-retard-list.page.scss'],
})
export class ModalRetardListPage implements OnInit {

    @Input() routeId: string;
    @Input() arretId: number;
    @Input() arretType: string;
    @Input() stopname: string;

    public retards: RetardDto[];
    public routeName: string;

    constructor(private restApiService: RestApiService,
                private modalCtrl: ModalController) {
    }

    ngOnInit() {
        console.log(this.routeId);
        this.getRetards();
        this.routeName = this.routeId.substring(3, this.routeId.length);
    }

    /**
     * Fonction permettant de récupérer les retards du jour sur l'arrêt actuel.
     */
    private getRetards() {
        this.restApiService.getRetard(this.routeId, this.arretId).subscribe(retards => this.retards = retards);
    }

    /**
     * Fonction permettant de fermer la page actuelle.
     */
    dismissModal() {
        this.modalCtrl.dismiss();
    }
}
