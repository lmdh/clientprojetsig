import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ModalAddPenuriePage } from './modal-add-penurie.page';

const routes: Routes = [
  {
    path: '',
    component: ModalAddPenuriePage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule
    ],
  declarations: [ModalAddPenuriePage]
})
export class ModalAddPenuriePageModule {}
