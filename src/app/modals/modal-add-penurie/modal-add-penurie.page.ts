import {Component, Input, OnInit} from '@angular/core';
import {LayerType} from '../../enums/LayerType';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertController, ModalController} from '@ionic/angular';
import {RestApiService} from '../../services/rest-api.service';
import {StationVeloDto} from '../../dto/StationVeloDto';
import {DegradationDto} from '../../dto/signalement/degradation/DegradationDto';
import {PenurieDto} from '../../dto/signalement/PenurieDto';

@Component({
    selector: 'app-modal-add-penurie',
    templateUrl: './modal-add-penurie.page.html',
    styleUrls: ['./modal-add-penurie.page.scss'],
})
export class ModalAddPenuriePage implements OnInit {

    @Input() station: StationVeloDto;
    addPenurieForm: FormGroup;
    title: string;

    constructor(public formBuilder: FormBuilder,
                private modalCtrl: ModalController,
                private restApiService: RestApiService,
                private alertController: AlertController) {
        // On définit les champs du formulaire et on indique si ils sont requis.
        this.addPenurieForm = this.formBuilder.group({
            message: ['', Validators.required]
        });
    }

    ngOnInit() {
        this.title = this.station.nomstation;
    }

    /**
     * Fonction permettant de fermer la page actuelle.
     */
    public dismissModal() {
        this.modalCtrl.dismiss();
    }

    /**
     * Méthode permettant d'ajouter une pénurie
     */
    public async addPenurie() {
        const penurieDto: PenurieDto = new PenurieDto();
        penurieDto.message = this.addPenurieForm.get('message').value;
        penurieDto.date = new Date();
        await this.restApiService.checkToken();
        this.restApiService.postPenurie(penurieDto, this.station.id).subscribe(() => {
            this.addPenurieResult();
        }, error => {
            this.addPenurieError(error);
        });
        console.log('test');
    }

    /**
     * Fonction permettant d'afficher l'alert de confirmation d'ajout de pénurie et de revenir vers la page de detail de la station vélo +.
     */
    private addPenurieResult() {
        this.modalCtrl.dismiss().then(() => {
                this.alertController.create({
                    header: 'Succès',
                    message: 'Signalement réussi !',
                    buttons: ['Ok']
                }).then(alert => alert.present());
            }
        );
    }

    /** Permet d'afficher une alert box contenant le message d'erreur passé en paramètre.
     * @param error Le message d'erreur à afficher.
     */
    private addPenurieError(error: any) {
        this.alertController.create({
            header: 'Echec',
            message: error,
            buttons: ['Ok']
        }).then(alert => alert.present());
    }
}
