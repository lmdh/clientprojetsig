import {Component, Input, OnInit} from '@angular/core';
import {CoupureLigneDto} from '../../dto/signalement/CoupureLigneDto';
import {RestApiService} from '../../services/rest-api.service';
import {ModalController} from '@ionic/angular';
import {ArretTaoDto} from '../../dto/ArretTaoDto';

@Component({
    selector: 'app-modal-coupure-list',
    templateUrl: './modal-coupure-list.page.html',
    styleUrls: ['./modal-coupure-list.page.scss'],
})
export class ModalCoupureListPage implements OnInit {

    @Input() routeId: string;
    coupures: CoupureLigneDto[];
    arrets: ArretTaoDto[];
    public routeName: string;

    constructor(private restApiService: RestApiService,
                private modalCtrl: ModalController) {
    }

    ngOnInit() {
        this.routeName = this.routeId.substring(3, this.routeId.length);
        this.getArrets();
        this.getCoupures();
    }

    /**
     * Fonction permettant de récupérer les coupures d'une ligne tao.
     */
    private getCoupures() {
        this.restApiService.getCoupures(this.routeId).subscribe(coupures => this.coupures = coupures);
    }

    /**
     * Fonction permettant de récupérer les arrêts de la ligne tao.
     */
    private getArrets() {
        this.restApiService.getArrets(this.routeId).subscribe(arrets => this.arrets = arrets);
    }

    /**
     * Fonction permettant de récupérer le nom d'un arrêt à l'aide de son id.
     * @param gid Id de l'arrêt tao.
     */
    getArretStopname(gid: number) {
        const arretTaoDto: ArretTaoDto = this.arrets.find(arret => arret.gid === gid);
        if (arretTaoDto !== null) {
            return arretTaoDto.stopname;
        } else {
            return gid;
        }
    }

    /**
     * Fonction permettant de fermer la page actuelle.
     */
    dismissModal() {
        this.modalCtrl.dismiss();
    }
}
