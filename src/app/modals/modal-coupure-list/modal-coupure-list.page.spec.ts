import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCoupureListPage } from './modal-coupure-list.page';

describe('ModalCoupureListPage', () => {
  let component: ModalCoupureListPage;
  let fixture: ComponentFixture<ModalCoupureListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCoupureListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCoupureListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
